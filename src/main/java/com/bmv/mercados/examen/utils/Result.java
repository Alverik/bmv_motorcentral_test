package com.bmv.mercados.examen.utils;

import java.util.ArrayList;
import java.util.List;

public class Result {

    private List<String> results = new ArrayList<>();
    protected static Result instance = new Result();

    public static void add(String x) {
        instance.results.add(x);
    }

    protected List<String> getResults() {
        return results;
    }

    protected void reset() {
        results.clear();
    }
}
