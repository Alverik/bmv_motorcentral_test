package com.bmv.mercados.examen.nodes;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.bmv.mercados.examen.utils.ResultTest.CONSOLE;

public class NodesTest {

    @After
    public void cleaner() {
        CONSOLE.reset();
    }

    @Test
    public void example1() {
        String[] lines = {"00", "0."};
        Map<String, List<String>> datas = new HashMap<>();
        datas.put("0,0", new ArrayList<String>(){{add("1,0"); add("0,1");}});
        datas.put("1,0", new ArrayList<String>(){{add("-1,-1"); add("-1,-1");}});
        datas.put("0,1", new ArrayList<String>(){{add("-1,-1"); add("-1,-1");}});
        validate(lines, datas);
    }

    @Test
    public void example2() {
        String[] lines = {"0.0", "000"};
        Map<String, List<String>> datas = new HashMap<>();
        datas.put("0,0", new ArrayList<String>(){{add("2,0"); add("0,1");}});
        datas.put("2,0", new ArrayList<String>(){{add("-1,-1"); add("2,1");}});
        datas.put("0,1", new ArrayList<String>(){{add("1,1"); add("-1,-1");}});
        datas.put("1,1", new ArrayList<String>(){{add("2,1"); add("-1,-1");}});
        datas.put("2,1", new ArrayList<String>(){{add("-1,-1"); add("-1,-1");}});
        validate(lines, datas);
    }

    @Test
    public void vertical() {
        String[] lines = {"00."};
        Map<String, List<String>> datas = new HashMap<>();
        datas.put("0,0", new ArrayList<String>(){{add("1,0"); add("-1,-1");}});
        datas.put("1,0", new ArrayList<String>(){{add("-1,-1"); add("-1,-1");}});
        validate(lines, datas);
    }

    @Test
    public void horizontal() {
        String[] lines = {"0", "0", "."};
        Map<String, List<String>> datas = new HashMap<>();
        datas.put("0,0", new ArrayList<String>(){{add("-1,-1"); add("0,1");}});
        datas.put("0,1", new ArrayList<String>(){{add("-1,-1"); add("-1,-1");}});
        validate(lines, datas);
    }

    @Test
    public void square() {
        String[] lines = {"0.0", "...", "0.0"};
        Map<String, List<String>> datas = new HashMap<>();
        datas.put("0,0", new ArrayList<String>(){{add("2,0"); add("0,2");}});
        datas.put("2,0", new ArrayList<String>(){{add("-1,-1"); add("2,2");}});
        datas.put("0,2", new ArrayList<String>(){{add("2,2"); add("-1,-1");}});
        datas.put("2,2", new ArrayList<String>(){{add("-1,-1"); add("-1,-1");}});
        validate(lines, datas);
    }

    @Test
    public void rectangle() {
        String[] lines = {"0.0.0", "0.00."};
        Map<String, List<String>> datas = new HashMap<>();
        datas.put("0,0", new ArrayList<String>(){{add("2,0"); add("0,1");}});
        datas.put("2,0", new ArrayList<String>(){{add("4,0"); add("2,1");}});
        datas.put("4,0", new ArrayList<String>(){{add("-1,-1"); add("-1,-1");}});
        datas.put("0,1", new ArrayList<String>(){{add("2,1"); add("-1,-1");}});
        datas.put("2,1", new ArrayList<String>(){{add("3,1"); add("-1,-1");}});
        datas.put("3,1", new ArrayList<String>(){{add("-1,-1"); add("-1,-1");}});
        validate(lines, datas);
    }

    private void validate(String[] lines, Map<String, List<String>> responses) {
        Nodes.findNeighbors(lines[0].length(), lines.length,lines);
        System.out.println("** TESTS **");
        Assert.assertEquals("Se esperaban " + responses.size() + " salidas a consola",
                responses.size(), CONSOLE.getResults().size());
        for(String line : CONSOLE.getResults()) {
            System.out.println(line);
            String[] tokens = line.split(" ");
            Assert.assertEquals("Formato incorrecto (x1,y1 x2,y2 x3,y3)", 3, tokens.length);
            String nodo = "Node " + tokens[0];
            Assert.assertTrue(nodo + " no soportado", responses.containsKey(tokens[0]));
            List<String> neighbors = responses.get(tokens[0]);
            Assert.assertEquals(nodo + "Nodo derecho incorrecto" + tokens[1] + ", esperado: " + neighbors.get(0),
                    tokens[1], neighbors.get(0));
            Assert.assertEquals(nodo + "Nodo abajo incorrecto" + tokens[2] + ", esperado: " + neighbors.get(1),
                    tokens[2], neighbors.get(1));
        }
    }

}