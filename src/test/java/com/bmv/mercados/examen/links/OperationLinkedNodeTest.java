package com.bmv.mercados.examen.links;

import org.junit.Assert;
import org.junit.Test;

public class OperationLinkedNodeTest {

    OperationLinkedNode operations = new OperationLinkedNode();
    LinkedNode init = null;

    @Test
    public void simple() {
        init = new LinkedNode(1);
        init.next = new LinkedNode(2);
        init.next.next = new LinkedNode(3);
        init.next.next.next = new LinkedNode(4);
        init.next.next.next.next = new LinkedNode(5);
        init.next.next.next.next.next = new LinkedNode(6);
        init.next.next.next.next.next.next = new LinkedNode(7);
        int toDelete = 2;
        LinkedNode result = operations.removeNodes(init, toDelete);
        Assert.assertTrue(validate(result, "1,3,4,5,6,7"));
    }

    @Test
    public void firstAndLast() {
        init = new LinkedNode(1);
        init.next = new LinkedNode(2);
        init.next.next = new LinkedNode(3);
        init.next.next.next = new LinkedNode(4);
        init.next.next.next.next = new LinkedNode(1);
        int toDelete = 1;
        LinkedNode result = operations.removeNodes(init, toDelete);
        Assert.assertTrue(validate(result, "2,3,4"));
    }

    @Test
    public void all() {
        init = new LinkedNode(1);
        init.next = new LinkedNode(1);
        init.next.next = new LinkedNode(1);
        init.next.next.next = new LinkedNode(1);
        int toDelete = 1;
        LinkedNode result = operations.removeNodes(init, toDelete);
        Assert.assertTrue(validate(result, ""));
    }

    @Test
    public void empty() {
        LinkedNode result = operations.removeNodes(init, 1);
        Assert.assertTrue(validate(result, ""));
    }


    public boolean validate(LinkedNode result, String expected) {
        String values = "";
        while(result != null) {
            values += result.getValue();
            result = result.next;
            if (result != null) {
                values += ",";
            } else {
                break;
            }
        }
        return values.equals(expected);
    }
}
