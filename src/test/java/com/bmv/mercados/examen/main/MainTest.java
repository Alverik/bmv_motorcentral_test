package com.bmv.mercados.examen.main;

import com.bmv.mercados.examen.utils.Result;
import com.bmv.mercados.examen.utils.ResultTest;
import org.junit.Assert;
import org.junit.Test;

public class MainTest {

    private ResultTest console = new ResultTest();

    @Test
    public void console() {
        Result.add("TEST");
        Assert.assertEquals(1, console.getResults().size());
        console.reset();
        Assert.assertEquals(0, console.getResults().size());
    }

}