package com.bmv.mercados.examen.defence;

import org.junit.Assert;
import org.junit.Test;

public class FormatValidatorTest {

    @Test
    public void incomplete() {
        String format = "][";
        Assert.assertFalse(new FormatValidator().validateFormat(format));
    }

    @Test
    public void simple() {
        String format = "[](){}";
        Assert.assertTrue(new FormatValidator().validateFormat(format));
    }

    @Test
    public void combined() {
        String format = "[({})]";
        Assert.assertTrue(new FormatValidator().validateFormat(format));
    }

    @Test
    public void mixed() {
        String format = "[(){}{()}]";
        Assert.assertTrue(new FormatValidator().validateFormat(format));
    }

    @Test
    public void incorrect() {
        String format = "[(){)]{()}";
        Assert.assertFalse(new FormatValidator().validateFormat(format));
    }

    @Test
    public void incorrect2() {
        String format = "[(){}{()}";
        Assert.assertFalse(new FormatValidator().validateFormat(format));
    }

    @Test
    public void large() {
        String format = "[()[]{()}{([([](){[]})])}]";
        Assert.assertTrue(new FormatValidator().validateFormat(format));
    }
}